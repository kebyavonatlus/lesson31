﻿namespace Lesson31
{
    public enum MaritalStatus
    {
        Married = 1,
        Single = 2,
        Divorce = 3
    }
}
