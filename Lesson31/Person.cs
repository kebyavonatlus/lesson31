﻿using System;

namespace Lesson31
{
    public class Person 
    {
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public Position Position { get; set; }

        public override string ToString()
        {
            return $"FullName: {FullName} MaritalStatus: {MaritalStatus} Gender: {Gender} Position: {Position}";
        }

        public static Random random { get; set; } = new Random();

        public static Person Generate(string name)
        {
            var person = new Person() { FullName = name };

            var position = random.Next(1, 3);

            person.Position = (Position)position;

            var gender = random.Next(10, 30);

            person.Gender = (Gender)gender;

            var maritalStatus = random.Next(1, 4);

            person.MaritalStatus = (MaritalStatus)maritalStatus;
            return person;
        }
    }
}
